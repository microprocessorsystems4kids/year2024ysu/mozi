#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

constexpr unsigned char SCREEN_WIDTH  = 128 ;
constexpr unsigned char SCREEN_HEIGHT = 64;
constexpr unsigned char OLED_RESET = 4;
constexpr unsigned char FLAP_BUTTON = 2;
constexpr unsigned char SPRITE_HEIGHT = 16;
constexpr unsigned char SPRITE_WIDTH = 16;
constexpr unsigned char INITIAL_GAME_SPEED = 100;



// Two frames of animation
constexpr unsigned char PROGMEM wing_down_bmp[] =
{ 
  B00000000, B00000000,
  B00000000, B00000000,
  B00000011, B11000000,
  B00011111, B11110000,
  B00111111, B00111000,
  B01111111, B11111110,
  B11111111, B11000001,
  B11011111, B01111110,
  B11011111, B01111000,
  B11011111, B01111000,
  B11001110, B01111000,
  B11110001, B11110000,
  B01111111, B11100000,
  B00111111, B11000000,
  B00000111, B00000000,
  B00000000, B00000000,
};

constexpr unsigned char PROGMEM wing_up_bmp[] =
{ 
  B00000000, B00000000,
  B00000000, B00000000,
  B00000011, B11000000,
  B00011111, B11110000,
  B00111111, B00111000,
  B01110001, B11111110,
  B11101110, B11000001,
  B11011111, B01111110,
  B11011111, B01111000,
  B11111111, B11111000,
  B11111111, B11111000,
  B11111111, B11110000,
  B01111111, B11100000,
  B00111111, B11000000,
  B00000111, B00000000,
  B00000000, B00000000,
};



Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

class Object { /* Abstract Class */
  protected:
    int x;
    int y;
  public:
    Object (int initialX, int initialY);
    virtual ~Object() = default; /* virtual destructor */
    inline int getX();
    inline int getY();
    virtual inline int getGapMomentum() = 0;
};

Object::Object (int initialX, int initialY) : x(initialX), y(initialY) {}

inline int Object::getX() { return x; }
inline int Object::getY() { return y; }

class Bird : public Object {
  private:
    int momentum;
  public:
    Bird(int initialX, int initialY);
    ~Bird() = default;
    void flap();
    void updatePosition();
    inline int getGapMomentum() final;
};


Bird::Bird(int initialX, int initialY) : Object(initialX, initialY), momentum(0) {}

void Bird::flap() {
  momentum = -4;
}

void Bird::updatePosition() {
  momentum += 1;
  y += momentum;

  if (y < 0) {
    y = 0;
  }
  else { /* do nothing */ }

  if (y > SCREEN_HEIGHT - SPRITE_HEIGHT) {
    y = SCREEN_HEIGHT - SPRITE_HEIGHT;
    momentum = -2;
  }
  else { /* do nothing */ }
}

inline int Bird::getGapMomentum() { return momentum; }

class Wall : public Object {
  private:
    int gap;
  public:
    Wall(int initialX, int initialY, int initialGap);
    ~Wall() = default;
    void moveLeft(int speed);
    inline int getGapMomentum() final;
};

Wall::Wall(int initialX, int initialY, int initialGap) : Object(initialX, initialY), gap(initialGap) {}

void Wall::moveLeft(int speed) {
  x -= speed;
  if (x + 10 < 0) { // Adjusted for wall_width of 10 pixels
    y = random(0, SCREEN_HEIGHT - gap);
    x = SCREEN_WIDTH;
  }
  else { /* do nothing */ }
}

inline int Wall::getGapMomentum() { return gap; }

class Game {
  private:
    Bird bird;
    Wall walls[2];
    int gameState;
    int score;
    int highScore;
    int gameSpeed;

  public:
    Game();
    ~Game() = default;
    /* declaration of methods */
    void setup(void);
    void update(void);
    void screenWipe(int speed);
    void textAt(int x, int y, String txt);
    void textAtCenter(int y, String txt);
    void outlineTextAtCenter(int y, String txt);
    void boldTextAtCenter(int y, String txt);

};

/* definition of the methods */
  Game::Game() : bird(SCREEN_WIDTH / 4, SCREEN_HEIGHT / 2), gameState(1), 
    score(0), highScore(0), gameSpeed(INITIAL_GAME_SPEED), 
    walls{{SCREEN_WIDTH, SCREEN_HEIGHT / 2 - 30, 30}, {SCREEN_WIDTH, SCREEN_HEIGHT / 2 - 15, 15}} 
   {/* supposed to be empty*/}

void Game::setup() {
  Serial.begin(9600);

  if (!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) {
    Serial.println(F("SSD1306 allocation failed"));
    for (;;);
  }

  display.setTextColor(WHITE);
  display.clearDisplay();

  pinMode(FLAP_BUTTON, INPUT_PULLUP);
  randomSeed(analogRead(0));
}

void Game::update() {
  if (gameState == 0) {
    // Game running
    display.clearDisplay();

    if (digitalRead(FLAP_BUTTON) == LOW) {
      bird.flap();
    }
    else { /* do nothing */ }

    bird.updatePosition();

    if (bird.getGapMomentum() < 0) {
      if (random(2) == 0) {
        display.drawBitmap(bird.getX(), bird.getY(), wing_down_bmp, SPRITE_WIDTH, SPRITE_HEIGHT, WHITE);
      } 
      else {
        display.drawBitmap(bird.getX(), bird.getY(), wing_up_bmp, SPRITE_WIDTH, SPRITE_HEIGHT, WHITE);
      }
    } 
    else {
      display.drawBitmap(bird.getX(), bird.getY(), wing_up_bmp, SPRITE_WIDTH, SPRITE_HEIGHT, WHITE);
    }

    for (int i = 0; i < 2; i++) {
      display.fillRect(walls[i].getX(), 0, 10, walls[i].getY(), WHITE);
      display.fillRect(walls[i].getX(), walls[i].getY() + walls[i].getGapMomentum(), 10, SCREEN_HEIGHT - walls[i].getY() + walls[i].getGapMomentum(), WHITE);

      if (bird.getX() == walls[i].getX()) {
        if (score > 1) {
          score += 2;
        } 
        else {
          score++;
        }

        highScore = max(score, highScore);

        if (score > 1) {
          gameSpeed = INITIAL_GAME_SPEED / 8;
        } 
        else {
          gameSpeed = INITIAL_GAME_SPEED;
        }
      }
      else { /* do nothing */ }

      if ((bird.getX() + SPRITE_WIDTH > walls[i].getX() && bird.getX() < walls[i].getX() + 10) && (bird.getY() < walls[i].getY() || bird.getY() + SPRITE_HEIGHT > walls[i].getY() + walls[i].getGapMomentum())) {
        gameState = 1;
      }
      else { /* do nothing */ }

      walls[i].moveLeft(4);
    }

    boldTextAtCenter(0, String(score));
    display.display();
    delay(gameSpeed);

  } 
  else {
    // Game over
    screenWipe(10);

    outlineTextAtCenter(1, "NANO BIRD");
    textAtCenter(SCREEN_HEIGHT / 2 - 8, "GAME OVER");
    textAtCenter(SCREEN_HEIGHT / 2, String(score));
    boldTextAtCenter(SCREEN_HEIGHT - 16, "HIGH SCORE");
    boldTextAtCenter(SCREEN_HEIGHT - 8, String(highScore));
    display.display();

    while (digitalRead(FLAP_BUTTON) == LOW);

    bird = Bird(SCREEN_WIDTH / 4, SCREEN_HEIGHT / 2);
    walls[0] = Wall(SCREEN_WIDTH, SCREEN_HEIGHT / 2 - 30, 30);
    walls[1] = Wall(SCREEN_WIDTH + SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2 - 30, 30);
    score = 0;
    gameSpeed = INITIAL_GAME_SPEED;

    while (digitalRead(FLAP_BUTTON) == HIGH);

    screenWipe(10);
    gameState = 0;
  }
}

void Game::screenWipe(int speed) {
  for (int i = 0; i < SCREEN_HEIGHT; i += speed) {
    display.fillRect(0, i, SCREEN_WIDTH, speed, WHITE);
    display.display();
  }

  for (int i = 0; i < SCREEN_HEIGHT; i += speed) {
    display.fillRect(0, i, SCREEN_WIDTH, speed, BLACK);
    display.display();
  }
}

void Game::textAt(int x, int y, String txt) {
  display.setCursor(x, y);
  display.print(txt);
}

void Game::textAtCenter(int y, String txt) {
  textAt(SCREEN_WIDTH / 2 - txt.length() * 3, y, txt);
}

void Game::outlineTextAtCenter(int y, String txt) {
  int x = SCREEN_WIDTH / 2 - txt.length() * 3;

  display.setTextColor(WHITE);
  textAt(x - 1, y, txt);
  textAt(x + 1, y, txt);
  textAt(x, y - 1, txt);
  textAt(x, y + 1, txt);

  display.setTextColor(BLACK);
  textAt(x, y, txt);
  display.setTextColor(WHITE);
}

void Game::boldTextAtCenter(int y, String txt) {
  int x = SCREEN_WIDTH / 2 - txt.length() * 3;

  textAt(x, y, txt);
  textAt(x + 1, y, txt);
}

void setup() { /* empty */ }

/* Program Loop */
void loop() {

  /* local object in local part of memory */
  Game game; 

  /* Setup of the game */
  game.setup();

  /* Game Loop */
  for (;;) {
    game.update();
  }
}
